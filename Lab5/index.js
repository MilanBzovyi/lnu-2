function isEquals(a, b) {
    return a === b;
}

function numberToString(num) {
    return Number(num).toString();
}

function storeNames(...a) {
    return Array.from(a);
}

function getDivision(f, s) {
    r = f / s;
    if (r < 1)
        return s / f;
    return r;
}

function negativeCount(arr) {
    res = 0;
    for (const e of arr) {
        if (e < 0)
            ++res;
    }
    return res;
}