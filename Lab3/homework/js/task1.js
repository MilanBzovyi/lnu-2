
function letch(){
    var check = parseInt(prompt('Input check number', 0));
    if (check < 0) {
        return(alert('Check can`t be less then zero'));
    } else if (isNaN(check)){
        return(alert("Invalid value"));
    }
    var tip = parseInt(prompt('Input tips percentage', 0));
    if (tip <= 0) {
        return(alert('Tip can`t be less then zero or minus percent'));
    } else if (tip > 100) {
        return(alert('Tip can`t be more then 100 '));
    } else if (isNaN(tip)){
        return(alert("Invalid value"));
    }
    var amount = ( check / 100 * tip);
    var result = check + amount; 
    alert(`Check number: ${check}
        Tip:${tip}%
        Tip amount:${amount}
        Total sum to pay:${result}
    `);
}
letch();


